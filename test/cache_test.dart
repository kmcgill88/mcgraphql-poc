import 'package:mcgraphql/mcgraphql.dart';
import 'package:test/test.dart';

void main() {
  final cachePolicy = CachePolicy(
      cacheKey: 'my key',
      ttl: Duration(minutes: 5),
      cacheStorageLocationPolicy: CacheStorageLocationPolicy.diskAndMemory);

  final json = {
    'data': {
      'roottag': {'name': 'Kevin'}
    }
  };

  group('cache', () {
    group('CachedQuery', () {
      test('can parse to and from', () {
        final cache = CachedQuery(
          cachePolicy: cachePolicy,
          data: json['data'],
          created: DateTime.now(),
        );
        final cacheJson = cache.toJson();
        expect(cacheJson, isNotEmpty);


        final parsedCache = CachedQuery.fromJson(cacheJson);

        expect(parsedCache.created, cache.created);
        expect(parsedCache.data, cache.data);
        expect(parsedCache.data, json['data']);
        expect(parsedCache.cachePolicy!.toJson(), cache.cachePolicy!.toJson());
      });
    });
  });
}
