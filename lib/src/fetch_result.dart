import 'package:mcgraphql/src/query_options.dart';

class FetchResult {
  FetchResult({
    this.statusCode,
    this.reasonPhrase,
    this.data,
    this.headers,
    this.options,
  });
  final int? statusCode;
  final String? reasonPhrase;
  /// List<dynamic> or Map<String, dynamic>
  final dynamic data;
  final Map<String, String>? headers;
  final BaseOptions? options;
}