import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';

class HttpService {
  HttpService({
    required this.url,
    required this.httpClient,
    this.getHeaders,
  });

  final FutureOr<Map<String, String>> Function()? getHeaders;
  final String url;
  final Client httpClient;

  Future<Response> post({
    required String? documentBody,
    Map<String, dynamic>? variables,
  }) async =>
      _withClient(
        (Client client) async {

          Map<String, String> asyncHeaders = getHeaders != null ? await getHeaders!() : {};

          /// https://graphql.org/learn/serving-over-http/
          final response = await client.post(
            Uri.parse(url),
            headers: {
              'Content-Type': 'application/json',
              ...asyncHeaders,
            },
            body: json.encode({
              'query': documentBody,
              if (variables != null) 'variables': variables
            }),
          );

          return response;
        },
      );

  Future<T> _withClient<T>(Future<T> fn(Client client)) async {
    try {
      return await fn(httpClient);
    } finally {
      // Release memory
      httpClient.close();
    }
  }
}
