// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'query_options.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QueryOptions _$QueryOptionsFromJson(Map<String, dynamic> json) {
  return QueryOptions(
    document: json['document'] as String,
    variables: json['variables'] as Map<String, dynamic>?,
    cachePolicy: json['cachePolicy'] == null
        ? null
        : CachePolicy.fromJson(json['cachePolicy'] as Map<String, dynamic>),
    fetchPolicy: _$enumDecode(_$FetchPolicyEnumMap, json['fetchPolicy']),
    errorPolicy: _$enumDecode(_$ErrorPolicyEnumMap, json['errorPolicy']),
  );
}

Map<String, dynamic> _$QueryOptionsToJson(QueryOptions instance) =>
    <String, dynamic>{
      'document': instance.document,
      'variables': instance.variables,
      'cachePolicy': instance.cachePolicy?.toJson(),
      'fetchPolicy': _$FetchPolicyEnumMap[instance.fetchPolicy],
      'errorPolicy': _$ErrorPolicyEnumMap[instance.errorPolicy],
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$FetchPolicyEnumMap = {
  FetchPolicy.cacheFirst: 'cacheFirst',
  FetchPolicy.cacheAndNetwork: 'cacheAndNetwork',
  FetchPolicy.cacheOnly: 'cacheOnly',
  FetchPolicy.noCache: 'noCache',
  FetchPolicy.networkOnly: 'networkOnly',
};

const _$ErrorPolicyEnumMap = {
  ErrorPolicy.none: 'none',
  ErrorPolicy.ignore: 'ignore',
  ErrorPolicy.all: 'all',
};

MutationOptions _$MutationOptionsFromJson(Map<String, dynamic> json) {
  return MutationOptions(
    document: json['document'] as String,
    variables: json['variables'] as Map<String, dynamic>?,
    cachePolicy: json['cachePolicy'] == null
        ? null
        : CachePolicy.fromJson(json['cachePolicy'] as Map<String, dynamic>),
    fetchPolicy: _$enumDecode(_$FetchPolicyEnumMap, json['fetchPolicy']),
    errorPolicy: _$enumDecode(_$ErrorPolicyEnumMap, json['errorPolicy']),
  );
}

Map<String, dynamic> _$MutationOptionsToJson(MutationOptions instance) =>
    <String, dynamic>{
      'document': instance.document,
      'variables': instance.variables,
      'cachePolicy': instance.cachePolicy?.toJson(),
      'fetchPolicy': _$FetchPolicyEnumMap[instance.fetchPolicy],
      'errorPolicy': _$ErrorPolicyEnumMap[instance.errorPolicy],
    };
