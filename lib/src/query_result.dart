import 'dart:async' show FutureOr;

import 'package:mcgraphql/src/exceptions.dart';
import 'package:json_annotation/json_annotation.dart';

part 'query_result.g.dart';

enum QueryResultSource {
  loading,
  cache,
//  OptimisticResult,
  network,
  unexecuted,
}

final eagerSources = {
  QueryResultSource.cache,
//  QueryResultSource.OptimisticResult
};

@JsonSerializable(explicitToJson: true)
class QueryResult {
  QueryResult({
    this.data,
    this.exception,
    this.loading = false,
    this.source = QueryResultSource.unexecuted,
  });

  DateTime _timestamp = DateTime.now();

  DateTime get timestamp => _timestamp;

  QueryResultSource? source;

  Map<String, dynamic>? data;

  OperationException? exception;

  bool? loading;

  bool get hasData => data != null && data!.isNotEmpty;

  bool get hasException => exception != null;
}

class MultiSourceResult {
  MultiSourceResult({
    required this.eagerResult,
    this.networkResult,
  });
  QueryResult eagerResult;
  FutureOr<QueryResult>? networkResult;
}
