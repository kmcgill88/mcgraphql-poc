// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cache.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CachedQuery _$CachedQueryFromJson(Map<String, dynamic> json) {
  return CachedQuery(
    cachePolicy: json['cachePolicy'] == null
        ? null
        : CachePolicy.fromJson(json['cachePolicy'] as Map<String, dynamic>),
    data: json['data'] as Map<String, dynamic>?,
    created: json['created'] == null
        ? null
        : DateTime.parse(json['created'] as String),
  );
}

Map<String, dynamic> _$CachedQueryToJson(CachedQuery instance) =>
    <String, dynamic>{
      'cachePolicy': instance.cachePolicy?.toJson(),
      'data': instance.data,
      'created': instance.created?.toIso8601String(),
    };

CachePolicy _$CachePolicyFromJson(Map<String, dynamic> json) {
  return CachePolicy(
    cacheKey: json['cacheKey'] as String?,
    ttl:
        json['ttl'] == null ? null : Duration(microseconds: json['ttl'] as int),
    cacheStorageLocationPolicy: _$enumDecode(
        _$CacheStorageLocationPolicyEnumMap,
        json['cacheStorageLocationPolicy']),
  );
}

Map<String, dynamic> _$CachePolicyToJson(CachePolicy instance) =>
    <String, dynamic>{
      'cacheStorageLocationPolicy': _$CacheStorageLocationPolicyEnumMap[
          instance.cacheStorageLocationPolicy],
      'ttl': instance.ttl?.inMicroseconds,
      'cacheKey': instance.cacheKey,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

const _$CacheStorageLocationPolicyEnumMap = {
  CacheStorageLocationPolicy.diskOnly: 'diskOnly',
  CacheStorageLocationPolicy.diskAndMemory: 'diskAndMemory',
  CacheStorageLocationPolicy.memoryOnly: 'memoryOnly',
  CacheStorageLocationPolicy.none: 'none',
};
