// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exceptions.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OperationException _$OperationExceptionFromJson(Map<String, dynamic> json) {
  return OperationException(
    graphqlErrors: (json['graphqlErrors'] as List<dynamic>)
        .map((e) => GraphQLError.fromJson(e as Map<String, dynamic>)),
  );
}

Map<String, dynamic> _$OperationExceptionToJson(OperationException instance) =>
    <String, dynamic>{
      'graphqlErrors': instance.graphqlErrors.map((e) => e.toJson()).toList(),
    };

GraphQLError _$GraphQLErrorFromJson(Map<String, dynamic> json) {
  return GraphQLError(
    raw: json['raw'],
    message: json['message'] as String?,
    locations: (json['locations'] as List<dynamic>?)
        ?.map((e) => Location.fromJson(e as Map<String, dynamic>))
        .toList(),
    path: json['path'] as List<dynamic>?,
    extensions: json['extensions'] as Map<String, dynamic>?,
  );
}

Map<String, dynamic> _$GraphQLErrorToJson(GraphQLError instance) =>
    <String, dynamic>{
      'raw': instance.raw,
      'message': instance.message,
      'locations': instance.locations?.map((e) => e.toJson()).toList(),
      'path': instance.path,
      'extensions': instance.extensions,
    };

Location _$LocationFromJson(Map<String, dynamic> json) {
  return Location(
    json['line'] as int?,
    json['column'] as int?,
  );
}

Map<String, dynamic> _$LocationToJson(Location instance) => <String, dynamic>{
      'line': instance.line,
      'column': instance.column,
    };
