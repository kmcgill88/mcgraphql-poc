// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'query_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QueryResult _$QueryResultFromJson(Map<String, dynamic> json) {
  return QueryResult(
    data: json['data'] as Map<String, dynamic>?,
    exception: json['exception'] == null
        ? null
        : OperationException.fromJson(
            json['exception'] as Map<String, dynamic>),
    loading: json['loading'] as bool?,
    source: _$enumDecodeNullable(_$QueryResultSourceEnumMap, json['source']),
  );
}

Map<String, dynamic> _$QueryResultToJson(QueryResult instance) =>
    <String, dynamic>{
      'source': _$QueryResultSourceEnumMap[instance.source],
      'data': instance.data,
      'exception': instance.exception?.toJson(),
      'loading': instance.loading,
    };

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$QueryResultSourceEnumMap = {
  QueryResultSource.loading: 'loading',
  QueryResultSource.cache: 'cache',
  QueryResultSource.network: 'network',
  QueryResultSource.unexecuted: 'unexecuted',
};
