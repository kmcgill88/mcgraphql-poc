import 'package:mcgraphql/mcgraphql.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cache.g.dart';

abstract class Cache {
  /// Read cached item
  ///
  Future<CachedQuery?> read(BaseOptions options) async {
    throw Exception('Cache read not implemented. You must supply a cache.');
  }

  /// Add data to the cache
  ///
  Future<void> write(BaseOptions options,
      Map<String, dynamic> data,) async {
    throw Exception('Cache read not implemented. You must supply a cache.');
  }

  /// Removes an item from the map.
  ///
  Future<CachedQuery?> remove(BaseOptions options) {
    throw Exception('Cache read not implemented. You must supply a cache.');
  }

  /// Removes all pairs from the map.
  /// After this, the map is empty.
  ///
  Future<void> clear() {
    throw Exception('Cache read not implemented. You must supply a cache.');
  }
}

abstract class DiskCache extends Cache {
  /// Save cache to disk
  ///
  Future<void> save() async {
    throw Exception('Cache read not implemented. You must supply a cache.');
  }

  /// Restore cache from Disk
  ///
  Future<void> restore() {
    throw Exception('Cache read not implemented. You must supply a cache.');
  }
}

class InMemoryCache implements Cache {
  Map<String, CachedQuery> cache = {};

  @override
  Future<CachedQuery?> read(BaseOptions options) async {
    if (cache.containsKey(options.cacheKey())) {
      /// It's in the cache, but is it too old?
      ///
      CachedQuery cachedQuery = cache[options.cacheKey()]!;

      if (!cachedQuery.ttlIsExpired) {
        return cachedQuery;
      }
    }

    return null;
  }

  @override
  Future<void> write(BaseOptions options, Map<String, dynamic> data) async {
    if (shouldSaveToMemoryCache(
      options.cachePolicy?.cacheStorageLocationPolicy,
    )) {
      cache[options.cacheKey()] = CachedQuery(
        cachePolicy: options.cachePolicy,
        data: data['data'],
        created: DateTime.now(),
      );
    }
  }

  @override
  Future<void> clear() async {
    cache.clear();
  }

  @override
  Future<CachedQuery?> remove(BaseOptions options) async {
    return cache.remove(options.cacheKey());
  }
}

@JsonSerializable(explicitToJson: true)
class CachedQuery {
  CachedQuery({
    this.cachePolicy,
    this.data,
    this.created,
  });

  /// How the user wants this data to be handled.
  ///
  CachePolicy? cachePolicy;

  /// The graphql payload
  ///
  Map<String, dynamic>? data;

  /// When the data was originally saved from the network
  ///
  DateTime? created;

  /// Convenience check
  ///
  bool get ttlIsExpired {
    if (cachePolicy!.ttl == null) {
      return false;
    }

    return DateTime
        .now()
        .difference(created!)
        .inMilliseconds >=
        cachePolicy!.ttl!.inMilliseconds;
  }

  factory CachedQuery.fromJson(Map<String, dynamic> json) =>
      _$CachedQueryFromJson(json);

  Map<String, dynamic> toJson() => _$CachedQueryToJson(this);
}

/// User defined criteria how to handle queries
///
@JsonSerializable(explicitToJson: true)
class CachePolicy {
  CachePolicy({
    this.cacheKey,
    this.ttl,
    this.cacheStorageLocationPolicy = CacheStorageLocationPolicy.diskAndMemory,
  });

  /// Where to put the data
  ///
  CacheStorageLocationPolicy cacheStorageLocationPolicy;

  /// Time-To-Live
  /// The Duration to keep in cache. When data passes it's ttl, fetch from network to replace
  /// inMilliseconds precision
  /// If ttl is null, it will never expire from cache
  Duration? ttl;

  /// Key to use for caching the query
  /// This must be unique.
  ///
  String? cacheKey;

  /// Convenience check
  ///
  bool get cacheKeyIsProvided => cacheKey != null && cacheKey!.isNotEmpty;

  factory CachePolicy.fromJson(Map<String, dynamic> json) =>
      _$CachePolicyFromJson(json);

  Map<String, dynamic> toJson() => _$CachePolicyToJson(this);
}

enum CacheStorageLocationPolicy {
  diskOnly,
  diskAndMemory,
  memoryOnly,
  none,
}

bool shouldSaveToMemoryCache(
    CacheStorageLocationPolicy? cacheStorageLocationPolicy) =>
    cacheStorageLocationPolicy == CacheStorageLocationPolicy.diskAndMemory ||
        cacheStorageLocationPolicy == CacheStorageLocationPolicy.memoryOnly;

bool shouldSaveToDiskCache(
    CacheStorageLocationPolicy cacheStorageLocationPolicy) =>
    cacheStorageLocationPolicy == CacheStorageLocationPolicy.diskAndMemory ||
        cacheStorageLocationPolicy == CacheStorageLocationPolicy.diskOnly;

bool shouldRemoveFromDisk(
    CacheStorageLocationPolicy cacheStorageLocationPolicy) =>
    shouldSaveToDiskCache(cacheStorageLocationPolicy);