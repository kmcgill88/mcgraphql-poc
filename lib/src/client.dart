import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart';
import 'package:mcgraphql/mcgraphql.dart';
import 'package:mcgraphql/src/cache.dart';
import 'package:mcgraphql/src/exceptions.dart';
import 'package:mcgraphql/src/fetch_result.dart';
import 'package:mcgraphql/src/network_service.dart';

class McGraphQlClient {
  McGraphQlClient({
    required this.url,
    this.client,
    this.getHeaders,
    this.cache,
    this.onFetchResponse,
  });

  /// Url to your graphql instance
  /// ie. https://cool.xyz/graphql
  ///
  String url;

  /// Headers for the HTTP requests
  /// Default headers than can be overwritten per mutation or query
  ///
  FutureOr<Map<String, String>> Function()? getHeaders;

  /// Callback for clients to inspect every http response
  /// ie. 401, need to re-auth user before continuing
  ///
  Future<QueryResult?> Function(
    FetchResult result,
    Future<QueryResult> Function(BaseOptions options) retry,
  )? onFetchResponse;

  /// HttpClient from http package
  ///
  Client? client;

  /// Use passed client, or create new
  ///
  Client get _getClient => client ?? Client();

  /// Http utility service
  ///
  HttpService get _httpService => HttpService(
        url: url,
        httpClient: _getClient,
        getHeaders: getHeaders,
      );

  /// Cache to use, if provided
  ///
  Cache? cache;

  bool get cacheIsProvided => cache != null;

  Future<QueryResult> query(QueryOptions options) async {
    try {
      final fetchPolicy = options.fetchPolicy;

      if (shouldCheckCache(fetchPolicy)) {
        try {
          final QueryResult cachedQueryResult = await _fetchFromCache(options);

          if (shouldFinishWhenFoundInCache(fetchPolicy)) {
            return cachedQueryResult;
          }
        } on NotFoundInCacheException {
          if (shouldFailIfNotInCache(fetchPolicy)) {
            rethrow;
          }
        }
      }

      return await _fetchFromNetwork(
        options,
        shouldCache: shouldCacheNetworkResult(fetchPolicy),
      );
    } on Exception catch (exception) {
      return QueryResult(
        data: null,
        exception: OperationException(
          clientException: exception,
        ),
        loading: false,
        source: QueryResultSource.network,
      );
    }
  }

  Future<QueryResult> mutate(MutationOptions options) async {
    try {
      /// No cached results, skip right to the network
      ///
      QueryResult queryResult = await _fetchFromNetwork(
        options,
        shouldCache: false,
      );

      if (options.onCompleted != null) {
        options.onCompleted!(options, cache, queryResult);
      }

      return queryResult;
    } on Exception catch (exception) {
      return QueryResult(
        data: null,
        exception: OperationException(
          clientException: exception,
        ),
        loading: false,
        source: QueryResultSource.network,
      );
    }
  }

  /// Attempt to fetch from the network
  ///
  Future<QueryResult> _fetchFromNetwork(BaseOptions options,
      {shouldCache = false}) async {
    final response = await _httpService.post(
      documentBody: options.document,
      variables: options.variables,
    );

    if (onFetchResponse != null) {
      final result = await onFetchResponse!(
        FetchResult(
          statusCode: response.statusCode,
          reasonPhrase: response.reasonPhrase,
          headers: response.headers,
          data: response.body,
          options: options,
        ),
        (BaseOptions options) =>
            _fetchFromNetwork(options, shouldCache: shouldCache),
      );
      if (result != null) {
        return result;
      }
    }

    final httpCodeDidFail = response.statusCode > 299;
    if (httpCodeDidFail && options.errorPolicy == ErrorPolicy.none) {
      throw Exception(
        'Http request failed with status code: ${response.statusCode}',
      );
    }

    final payload = Map<String, dynamic>.from(jsonDecode(response.body));

    /// Check for gql errors
    ///
    if (payload.containsKey('errors') &&
        options.errorPolicy == ErrorPolicy.none) {
      return QueryResult(
        data: null,
        exception: OperationException(
          graphqlErrors: [
            GraphQLError.fromJSON(payload['errors'][0]),
          ],
        ),
        loading: false,
        source: QueryResultSource.network,
      );
    }

    if (shouldCache && cacheIsProvided) {
      await cache!.write(options, payload);
    }

    /// Happy Path
    ///
    return QueryResult(
      data: payload['data'],
      loading: false,
      source: QueryResultSource.network,
    );
  }

  Future<QueryResult> _fetchFromCache(BaseOptions options) async {
    if (cacheIsProvided) {
      final CachedQuery? cachedData = await cache!.read(options);

      if (cachedData != null) {
        return QueryResult(
          data: cachedData.data,
          loading: false,
          source: QueryResultSource.cache,
        );
      }
    }

    throw NotFoundInCacheException();
  }
}
