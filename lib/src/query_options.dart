import 'dart:convert';

import 'package:mcgraphql/src/cache.dart';
import 'package:mcgraphql/src/query_result.dart';
import 'package:json_annotation/json_annotation.dart';

part 'query_options.g.dart';

/// [FetchPolicy] determines where the client may return a result from. The options are:
/// - cacheFirst (default): return result from cache. Only fetch from network if cached result is not available.
/// - cacheAndNetwork: return result from cache first (if it exists), then return network result once it's available.
/// - cacheOnly: return result from cache if available, fail otherwise.
/// - noCache: return result from network, fail if network call doesn't succeed, don't save to cache.
/// - networkOnly: return result from network, fail if network call doesn't succeed, save to cache.
enum FetchPolicy {
  cacheFirst,
  cacheAndNetwork,
  cacheOnly,
  noCache,
  networkOnly,
}

bool shouldFailIfNotInCache(FetchPolicy? fetchPolicy) =>
    fetchPolicy == FetchPolicy.cacheOnly;

bool shouldCacheNetworkResult(FetchPolicy? fetchPolicy) =>
    fetchPolicy != FetchPolicy.noCache;

bool shouldFinishWhenFoundInCache(FetchPolicy? fetchPolicy) =>
    fetchPolicy == FetchPolicy.cacheFirst ||
    fetchPolicy == FetchPolicy.cacheOnly;

bool shouldCheckCache(FetchPolicy? fetchPolicy) =>
    fetchPolicy == FetchPolicy.cacheFirst ||
    fetchPolicy == FetchPolicy.cacheAndNetwork ||
    fetchPolicy == FetchPolicy.cacheOnly;

/// [ErrorPolicy] determines the level of events for errors in the execution result. The options are:
/// - none (default): Any GraphQL Errors are treated the same as network errors and any data is ignored from the response.
/// - ignore:  Ignore allows you to read any data that is returned alongside GraphQL Errors,
///  but doesn't save the errors or report them to your UI.
/// - all: Using the all policy is the best way to notify your users of potential issues while still showing as much data as possible from your server.
///  It saves both data and errors into the Apollo Cache so your UI can use them.
enum ErrorPolicy {
  none,
  ignore,
  all,
}

abstract class BaseOptions {
  BaseOptions({
    required String this.document,
    Map<String, dynamic>? this.variables,
    CachePolicy? this.cachePolicy,
    required FetchPolicy this.fetchPolicy,
    required ErrorPolicy this.errorPolicy,
  });

  String document;
  Map<String, dynamic>? variables;
  CachePolicy? cachePolicy;
  FetchPolicy fetchPolicy;
  ErrorPolicy errorPolicy;

  String cacheKey() => cachePolicy?.cacheKey ?? (jsonEncode(variables ?? {}) + '-$document');
}

@JsonSerializable(explicitToJson: true)
class QueryOptions extends BaseOptions {
  QueryOptions({
    required String document,
    Map<String, dynamic>? variables,
    CachePolicy? cachePolicy,
    FetchPolicy fetchPolicy = FetchPolicy.cacheFirst,
    ErrorPolicy errorPolicy = ErrorPolicy.none,
  }) : super(
          document: document,
          variables: variables,
          cachePolicy: cachePolicy ??
              CachePolicy(cacheKey: '${jsonEncode(variables ?? {})}-$document'),
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
        );

  factory QueryOptions.fromJson(Map<String, dynamic> json) =>
      _$QueryOptionsFromJson(json);

  Map<String, dynamic> toJson() => _$QueryOptionsToJson(this);
}

typedef OnMutationCompleted = void Function(
    MutationOptions options, Cache? cache, QueryResult result);

@JsonSerializable(explicitToJson: true)
class MutationOptions extends BaseOptions {
  MutationOptions({
    required String document,
    OnMutationCompleted? onMutationCompleted,
    Map<String, dynamic>? variables,
    CachePolicy? cachePolicy,
    FetchPolicy fetchPolicy = FetchPolicy.networkOnly,
    ErrorPolicy errorPolicy = ErrorPolicy.none,
  }) : super(
          document: document,
          variables: variables,
          fetchPolicy: fetchPolicy,
          errorPolicy: errorPolicy,
          cachePolicy: cachePolicy ??
              CachePolicy(
                cacheKey: '${jsonEncode(variables ?? {})}-$document',
                cacheStorageLocationPolicy: CacheStorageLocationPolicy.none,
              ),
        ) {
    onCompleted = onMutationCompleted;
  }

  @JsonKey(ignore: true)
  OnMutationCompleted? onCompleted;

  factory MutationOptions.fromJson(Map<String, dynamic> json) =>
      _$MutationOptionsFromJson(json);

  Map<String, dynamic> toJson() => _$MutationOptionsToJson(this);
}
