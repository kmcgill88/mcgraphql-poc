import 'dart:io';

import 'package:mcgraphql/mcgraphql.dart';

import 'acceptance_util.dart';

void main() async {
  final client = getClient();
  final notificationId = '28ch02b2jobeougurbvoj';
  final deviceId = 'djklaf8280v0880v08wv0w';

  final createResult = await client.mutate(
    MutationOptions(
      document: createNotificationMutation,
      variables: {
        'notification': {
          'breed': null,
          'gender': 'Male',
          'location': null,
          'age': null,
          'species': 'Cat',
          'os': 'ios',
          'organization': 'accofnyc',
          'deviceToken': '89f2082309hf0298hf08ihwojdbjk2hg2',
          'notificationId': notificationId,
          'deviceId': deviceId,
        },
      },
      onMutationCompleted: (MutationOptions options, Cache? cache, QueryResult result) {
        print('called onResult for create. $cache and $result');
        if (cache == null) {
          exit(9999);
        }
      }
    ),
  );

  if (createResult.hasException) {
    throw createResult.exception!;
  }
  print(createResult.data);
  print('Mutation worked. Waiting for it to be updated by backend....');
  await Future.delayed(Duration(seconds: 5));
  print('Done waiting.');


  final updateResult = await client.mutate(
    MutationOptions(
      document: updateNotificationMutation,
      variables: {
        'notification': {
          'gender': 'Female',
          'notificationId': notificationId,
        },
      },
    ),
  );

  if (updateResult.hasException) {
    throw updateResult.exception!;
  }
  print(updateResult.data);
  print('Mutation worked. Waiting for it to be updated by backend....');
  await Future.delayed(Duration(seconds: 5));
  print('Done waiting.');




  final deleteResult = await client.mutate(
    MutationOptions(
      document: deleteNotificationMutation,
      variables: {
        'id': notificationId,
      },
    ),
  );

  if (deleteResult.hasException) {
    throw deleteResult.exception!;
  }
  print(deleteResult.data);
  print('Mutation worked. Waiting for it to be updated by backend....');
  print('All done here.');
}

final createNotificationMutation = """
mutation AcceptanceCreateNotificationMutation(\$notification: CreateNotificationInput!) {
  createNotification(notification: \$notification) {
    __typename
    notificationId
    deviceId
    deviceToken
    os
    species
    age
    gender
    breed
    location
    weight
  }
}
""";
final updateNotificationMutation = """
mutation AcceptanceUpdateNotificationMutation(\$notification: UpdateNotificationInput!) {
  updateNotification(notification: \$notification) {
    __typename
    notificationId
    deviceId
    deviceToken
    os
    species
    age
    gender
    breed
    location
    weight
  }
}
  """;
final deleteNotificationMutation = """
mutation AcceptanceDeleteNotificationMutation(\$id: String!) {
  deleteNotification(id: \$id)
}
""";
