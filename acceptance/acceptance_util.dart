import 'package:mcgraphql/mcgraphql.dart';

final jwt =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXZpY2VJZCI6ImFiYzEyMyIsIm9yZyI6ImFjY29mbnljIiwiaWF0IjoxNTc4MTk0MjkzLCJleHAiOjE1ODA3ODYyOTMsImlzcyI6InBldHMubWNnaWxsZGV2dGVjaC5jb20ifQ.qpOTBgU0eWALR7MubzaqaTGFiKVvCwEaWdtTnplys0Y';


final client = McGraphQlClient(
  url: 'https://pets.dev.mcgilldevtech.com/graphql',
  getHeaders: () => {
    'x-api-key': 'Pyl5AcFz181tr0FnrQIDQ5mAWkIdvOm82FjSnOi8',
    'Authorization': 'Bearer $jwt',
  },
  cache: InMemoryCache(),
);

final noJwtClient = McGraphQlClient(
  url: 'https://pets.dev.mcgilldevtech.com/graphql',
  getHeaders: () => {
    'x-api-key': 'Pyl5AcFz181tr0FnrQIDQ5mAWkIdvOm82FjSnOi8',
    'Authorization': 'Bearer 1234',
  },
  cache: InMemoryCache(),
);

McGraphQlClient getClient() => client;

McGraphQlClient getNoJwtClient() => noJwtClient;
