import 'dart:io';

import 'package:mcgraphql/mcgraphql.dart';

import 'acceptance_util.dart';

void main() async {
  final client = getClient();

  await queryPets(client);
  await queryLocationsANDSpecies(client);
  await queryYouTube(client);
  await shouldFail(getNoJwtClient());
  await shouldCallOnFetch();
}

Future<void> queryPets(McGraphQlClient client) async {
  final result = await client.query(QueryOptions(
    document: petsQuery,
  ));

  if (result.hasException) {
    print('EXCEPTION!!!! - queryPets' + result.exception.toString());
    exit(9999);
  }

  print(result.data!['feed']['pets'][0]['name']);
}

Future<void> queryLocationsANDSpecies(McGraphQlClient client) async {
  final result = await client.query(QueryOptions(
    document: locationsAndSpecies,
  ));

  if (result.hasException) {
    print('EXCEPTION!!!! - queryPets' + result.exception.toString());
    exit(9999);
  }

  print(result.data!['locations'][0]['name']);
  print(result.data!['species'][0]['name']);
}

Future<void> queryYouTube(McGraphQlClient client) async {
  final result = await client.query(
    QueryOptions(
      document: youtubeQuery,
      variables: {
        "ids": [
          "Dqcb-dFzmzc",
        ],
      },
    ),
  );

  if (result.hasException) {
    print('EXCEPTION!!!! - queryPets' + result.exception.toString());
    exit(9999);
  }

  print(result.data!['youtubeVideos'][0]['url']);
}

Future<void> shouldFail(McGraphQlClient client) async {
  final result = await client.query(QueryOptions(
    document: locationsAndSpecies,
  ));

  if (result.hasException) {
    print('This was suppose to fail.');
  } else {
    exit(9999);
  }
}

Future<void> shouldCallOnFetch() async {
  final client = McGraphQlClient(
      url: 'https://pets.dev.mcgilldevtech.com/graphql',
      getHeaders: () => {
            'x-api-key': '123',
            'Authorization': 'Bearer 123',
          },
      onFetchResponse: (
        FetchResult fetchResult,
        Future<QueryResult> Function(BaseOptions options) retry,
      ) async {
        if (fetchResult.statusCode == 401) {
          print('Got the 401 that was expected in callback.');
        } else {
          exit(9999);
        }

        /// Return null to skip and resume default behavior.
        ///
        return null;
      });

  final result = await client.query(QueryOptions(
    document: locationsAndSpecies,
  ));

  if (result.hasException) {
    print('This was suppose to fail with a 401.');
  } else {
    exit(9999);
  }
}

final locationsAndSpecies = """
query {
  locations {
    id
    name
    type
    address
    city
    state
  }
  species {
    name
    breeds
  }
}
""";

final petsQuery = """
query {
  feed {
    __typename
    updated
    pets {
      __typename
      id
      name
      age
      type
      species
      link
      gender
      summary
      size
      weight
      location
      spayedNeutered
      colors
      breeds
      photos
      youTubeIds
    }
  }
}
""";

final youtubeQuery = """
query GetYouTubeVideos(\$ids: [String!]!) {
  youtubeVideos(ids: \$ids) {
    id
    url
  }
}
""";
